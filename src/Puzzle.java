public class Puzzle {

   //Used materials
   //https://en.wikipedia.org/wiki/Backtracking
   //https://www.youtube.com/watch?v=DKCbsiDBN6c
   //https://intellipaat.com/community/6131/differences-between-backtracking-and-brute-force-search
   //https://gist.github.com/AhmadElsagheer/d96d1081af545834e32f3624b06c762a
   //https://stackoverflow.com/questions/44119627/differences-between-backtracking-and-brute-force-search
   //https://stackoverflow.com/questions/24853/c-what-is-the-difference-between-i-and-i


   public static void main(String[] args) {
      isCorrectInput(args);
      getVariates(args[0], args[1], args[2]);

   }

   //Properties
   private static boolean[] usedLetterArray;
   private static boolean[] usedDigitArray;
   private static int[] assignedDigitArray;

   /**
    * Check an input if it is in correct form, if not throws RuntimeException
    * @param a input as String[]
    */
   private static void isCorrectInput(String[] a) {
      //Checks for amount elements given
      if (a.length != 3) throw new RuntimeException(
              "Input elements amount is not right (Expected 3, got " + a.length +")");

      //Checks each element's length in input array
      for (String s :
              a) {
         if (s.length() > 16) throw new RuntimeException("Word '" + s + "' length is bigger than 16 symbols");
      }
   }

   /**
    * Main function
    * @param w1 first word
    * @param w2 second word
    * @param w3 third word
    */
   public static void getVariates(String w1, String w2, String w3) {

      //Initiate a array of booleans, each boolean for each Letter in (English) alphabet
      usedLetterArray = new boolean[26];
      //Initiate a array of booleans, where each index + 1 equals to digit and shows is it used or not
      usedDigitArray = new boolean[10];
      //Digit for each letter in (English) alphabet
      assignedDigitArray = new int[26];

      //Marks all letters given as used in usedLetterArray
      markLettersFromWord(w1);
      markLettersFromWord(w2);
      markLettersFromWord(w3);

      findSolutions(0, w1, w2, w3);

   }

   /**
    * Backtrack function which gets solutions
    * @param letterIndex index of letter
    * @param w1 first letter
    * @param w2 second letter
    * @param w3 third letter
    */
   private static void findSolutions(int letterIndex, String w1, String w2, String w3) {
      if (letterIndex == 26) {
         //Letters are assigned, so checking for condition
         if(checkCondition(w1, w2, w3)) {
            //Printing it out
            System.out.println("Found a solution!");
            for (int i = 0; i < 26; i++) {
               if (usedLetterArray[i])
                  System.out.print("[" + (char) (i + 'A') + " = " + assignedDigitArray[i] + "]");
            }
            System.out.println("\n------");
         }
         return;
      }
      // If letter is not used then skip it
      if (!usedLetterArray[letterIndex]) {
         //Increment letterIndex by one
         findSolutions(letterIndex + 1, w1, w2, w3);
         return;
      }

      //Trying to assign different digits to this letter
      for (int digit = 0; digit < 10; digit++) {

         //Digit cannot be used more than 1 time
         if (!usedDigitArray[digit]) {
            usedDigitArray[digit] = true;
            assignedDigitArray[letterIndex] = digit;
            findSolutions(letterIndex + 1, w1, w2, w3);
            usedDigitArray[digit] = false;
         }
      }
   }

   /**
    * Gets a word value example: A.value * 1000 + B.value * 100 + C.value * 10 + D.value
    * @param word to get value of
    * @return int, A.value * 1000 + B.value * 100 + C.value * 10 + D.value
    */
   public static int getWordValue(String word) {
      int r = 0;
      //Iterating through word letters
      //We would get result as A.value * 1000 + B.value * 100 + C.value * 10 + D.value
      for (int i = 0; i < word.length(); i++) {
         //Grow r * 10
         r = r * 10;

         //Doing the same trick with char
         // 'A' as int equal to ASCII code
         r += assignedDigitArray[word.charAt(i) - 'A'];
      }
      return r;

   }


   /**
    * Checks for condition w1.value + w2.value == w3.value
    * @param w1 first word
    * @param w2 second word
    * @param w3 third word
    * @return boolean, true if correct
    */
   public static boolean checkCondition(String w1, String w2, String w3){
      //First letter cannot be equal to zero (word[0] != 0)
      if (firstLetterValueIsZero(w1) || firstLetterValueIsZero(w2) || firstLetterValueIsZero(w3)) {
         return false;
      }
      //Checks condition
      return getWordValue(w1) + getWordValue(w2) == getWordValue(w3);
   }

   /**
    * Checks if first letter have value = 0
    * @param word to get first letter
    * @return boolean if first letter value equal 0 or not
    */
   static boolean firstLetterValueIsZero(String word) {
      //First letter cannot be equal 0, checks it with char indexes
      // example first letter is 'B' so 'B' - 'A'(66 -65 = 1) = 1, It is index of letter in out property array
      return assignedDigitArray[word.charAt(0) - 'A'] == 0;
   }

   /**
    * Marks each letter from given word in array as True(used)
    * @param word String given
    */
   public static void markLettersFromWord(String word) {
      //Iterate trough letters
      for(int i = 0; i < word.length(); i++)
         //Marks letter in array as true (used)
         //Chars can be used in index, it converts into ASCII CODE
         // (int) 'A' == 65
         // (int) 'B' == 66
         //So if we 'B' - 'A' we would get 1, 1 is our index for B letter in Property arrays
         usedLetterArray[word.charAt(i) - 'A'] = true;
   }


}